# TF code base
## Introduction

This pipeline is used to create an Amazon EKS (Elastic Kubernetes Service)
Amazon EKS is a Kubernetes Cluster platform that automates the deployment and management of Kubernetes applications on the AWS cloud. It automatically runs and scales the Kubernetes application containers across the multiple AWS cloud zones. 

Amazon Elastic Kubernetes Service Cluster will be created with the following AWS resources:
AWS Virtual Private Cloud (VPC)
Three public and three private AWS Subnets in different AWS Cloud availability zones.
# One AWS Route Table.
# Two AWS Route Table Association.
# AWS Internet Gateway attached to the VPC.
# AWS EKS Cluster. It will have one master node to manage the Kubernetes application.
# AWS EKS Node Group with two worker nodes.
# AWS Security Group with an Ingress rule.
# IAM Role for the AWS EKS Cluster with two policies.
# IAM Role for the Node Group with three policies.


To build the repository:
1. Clone the repository
    git clone https://gitlab.com/vinay.vijay.joshi/aws-eks-terraform.git

2. Setup your AWS credentials on your terminal
        export AWS_ACCESS_KEY_ID=<Key value> 
        export AWS_SECRET_ACCESS_KEY=<Secret Access Key value>
        export AWS_DEFAULT_REGION=us-west-2   

    Setup your AWS credentials in your gitlab pipeline, follow below link.
        https://docs.gitlab.com/ee/ci/cloud_deployment/

3. Commit to your pipeline and it will run

4. To destroy the components on AWS, commit the pipeline with commit message of "destroy"

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.46.0 |
| <a name="requirement_cloudinit"></a> [cloudinit](#requirement\_cloudinit) | ~> 2.2.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.16.1 |
| <a name="requirement_random"></a> [random](#requirement\_random) | ~> 3.4.3 |
| <a name="requirement_tls"></a> [tls](#requirement\_tls) | ~> 4.0.4 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.46.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.4.3 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_eks"></a> [eks](#module\_eks) | terraform-aws-modules/eks/aws | 19.0.4 |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws | 3.14.2 |

## Resources

| Name | Type |
|------|------|
| [random_string.suffix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [aws_availability_zones.available](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_region"></a> [region](#input\_region) | AWS region | `string` | `"us-west-2"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster_endpoint"></a> [cluster\_endpoint](#output\_cluster\_endpoint) | Endpoint for Amazon Web Service EKS |
| <a name="output_cluster_name"></a> [cluster\_name](#output\_cluster\_name) | Amazon Web Service EKS Cluster Name |
| <a name="output_cluster_security_group_id"></a> [cluster\_security\_group\_id](#output\_cluster\_security\_group\_id) | Security group ID for the Amazon Web Service EKS Cluster |
| <a name="output_region"></a> [region](#output\_region) | Amazon Web Service EKS Cluster region |
<!-- END_TF_DOCS -->  
## Footer
Contributor Names